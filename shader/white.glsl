#version 450

layout(location = 0) out vec4 colour;

layout(location = 1) in vec2 iResolution;
layout(location = 2) in float iTime;

void main() {
    colour = vec4(1.0);
}