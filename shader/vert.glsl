#version 450

layout(location = 0) in vec2 position;
layout(location = 0) out vec2 fragCoord;

layout(push_constant) uniform UniformBuffer {
    float time;
    vec2 resolution;
    vec2 mouse;
} push_constant;

layout(location = 1) out vec2 iResolution;
layout(location = 2) out float iTime;
layout(location = 3) out vec2 iMouse;

void main() {
    iResolution = push_constant.resolution;
    iTime = push_constant.time;
    iMouse = push_constant.mouse;

    fragCoord = (position - 1.0) / -2.0 * push_constant.resolution;
    gl_Position = vec4(position, 0.0, 1.0);
}