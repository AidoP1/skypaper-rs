#version 450

layout(location = 0) in vec2 fragCoord;
layout(location = 0) out vec4 fragColor;

layout(location = 1) in vec2 iResolution;
layout(location = 2) in float iTime;
layout(location = 3) in vec2 iMouse;

vec4 circle(vec4 colour, vec2 position, float radius) {
    if (length(gl_FragCoord.xy - position) < radius) {
        return colour;
    } else {
     	return vec4(0.0);   
    }
}

void main()
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    // Time varying pixel color
    vec3 col = 0.5 + 0.5*cos(0.5*iTime+uv.xyx+vec3(0,2,4));
    
    const float r = 70.0;
    const float pr = 40.0;
    const vec2 space = vec2(100.0, 0.0);
    const float mouth = 80.0;
    const float mouthr = 60.0;

    // Output to screen
    fragColor = vec4(col,1.0)
        + circle(vec4(1.0), iResolution.xy / 2.0 - space, r)
        - circle(vec4(1.0), clamp(length(-iMouse.xy + (iResolution.xy / 2.0 - space)), -(pr / 2.0 + 5.0), pr / 2.0 + 5.0) * normalize(iMouse.xy - (iResolution.xy / 2.0 - space)) + (iResolution.xy / 2.0 - space), pr)
        + circle(vec4(1.0), iResolution.xy / 2.0 + space, r)
        - circle(vec4(1.0), clamp(length(-iMouse.xy + (iResolution.xy / 2.0 + space)), -(pr / 2.0 + 5.0), pr / 2.0 + 5.0) * normalize(iMouse.xy - (iResolution.xy / 2.0 + space)) + (iResolution.xy / 2.0 + space), pr)
        
        + clamp(circle(vec4(1.0), iResolution.xy / 2.0 + vec2(0.0, mouth), mouthr) - circle(vec4(1.0), iResolution.xy / 2.0 + vec2(0.0, mouth - mouth * 0.6), mouthr * 1.3), 0.0, 1.0);
}