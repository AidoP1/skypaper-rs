#!/bin/sh

cd shader

glslc -fshader-stage=vert vert.glsl -o vert.spv
glslc -fshader-stage=frag tests/umbrella.glsl -o frag.spv