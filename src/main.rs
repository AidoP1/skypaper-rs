#![allow(unused_variables)]
#![allow(dead_code)]

pub mod windowing;
use windowing::Event;
pub mod graphics;

mod prelude {
    pub use super::windowing;
    
    #[derive(Debug)]
    pub struct Size {
        pub width: u16,
        pub height: u16,
        pub x: u16,
        pub y: u16
    }
    
    pub struct MousePosition {
        pub x: i32,
        pub y: i32
    }
}
use prelude::MousePosition;

mod ffi {
    pub use std::ffi::c_void as Void;
    pub use std::ffi::CString;
    pub use std::ptr::{null,null_mut};
    /// Create a strongly-typed C-style enum
    #[macro_export]
    macro_rules! c_enum {
        ($enum:ident -> $type:ty = [$($name:ident: $value:expr),*]) => {
            #[repr(transparent)]
            #[derive(Copy, Clone, PartialEq, Eq, Debug)]
            pub struct $enum($type);
            #[allow(non_upper_case_globals)]
            impl $enum {
                $(
                    pub const $name: Self = Self($value);
                )*
            }
            impl std::ops::Deref for $enum {
                type Target = $type;
                fn deref(&self) -> &Self::Target {
                    &self.0
                }
            }
        };
    }
}

fn main() {
    let mut windowing = windowing::Platform::new().expect("Error in windowing system");
    let mut graphics = graphics::Platform::new(&windowing);
    
    use std::time::{Duration,Instant};
    let last_instant = Instant::now();
    let epoch = last_instant;
    let target_frame_rate = Duration::from_millis((1.0 / 60.0) as _);

    'running: loop {
        if let Some(event) = windowing.event_pump() {
            match event {
                Event::Quit => break 'running,
                Event::Resize(width, height) => graphics.resize(width, height),
                Event::Nothing => ()
            }
        } else {
            // Only draw when no events are queued as rendering can block for too long. Prevents significant delay on the quit event
            let delta = Instant::now() - last_instant;
            if target_frame_rate > delta {
                std::thread::sleep(target_frame_rate - delta);
            }
            let size = windowing.size().unwrap();
            let MousePosition { x, y, ..} = windowing.mouse_position().unwrap();
            if graphics.ready(&size) && graphics.redraw(epoch, [x as f32, y as f32]) {
                graphics.resize(size.width as u32, size.height as u32)
            }
        }
    }
}
