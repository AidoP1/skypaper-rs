#[cfg(all(target_family = "unix", not(target_os = "macos")))]
mod xcb;
#[cfg(all(target_family = "unix", not(target_os = "macos")))]
pub use xcb::Xcb as Platform;

#[cfg(target_os = "windows")]
mod win32;
#[cfg(target_os = "windows")]
pub use win32::Win32 as Platform;

pub enum Event {
    Quit,
    Resize(u32, u32),
    Nothing
}
