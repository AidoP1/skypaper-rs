use crate::prelude::*;

use ash::util::*;
use ash::{vk, Entry, Instance, Device, version::EntryV1_0, version::InstanceV1_0, version::DeviceV1_0};
use ash::extensions::khr::{Surface, Swapchain};

const VERTICES: [[f32; 2]; 4] = [
    [-1.0, -1.0],
    [-1.0, 1.0],
    [1.0, -1.0],
    [1.0, 1.0]
];

#[repr(C)]
struct PushConstants {
    time: f32,
    _pad: [u8; 4],
    resolution: [f32; 2],
    mouse: [f32; 2],
}
impl PushConstants {
    fn new(time: f32, resolution: vk::Extent2D, mouse: [f32; 2]) -> Self {
        Self {
            time,
            _pad: [0; 4],
            resolution: [
                resolution.width as _,
                resolution.height as _
            ],
            mouse
        }
    }
    fn as_bytes(&self) -> &[u8] {
        unsafe { std::slice::from_raw_parts(self as *const _ as _, std::mem::size_of::<Self>()) }
    }
}

pub fn find_memory_type(memory_requirements: vk::MemoryRequirements, memory_properties: vk::PhysicalDeviceMemoryProperties, needing: vk::MemoryPropertyFlags) -> u32 {
    for index in 0..memory_properties.memory_type_count {
        if memory_requirements.memory_type_bits & (1 << index) != 0 && memory_properties.memory_types[index as usize].property_flags & needing == needing {
            return index
        }
    }
    panic!("Unable to find suitable memory type")
}

struct Pipeline {
    device: Device,

    vertex_shader: vk::ShaderModule,
    fragment_shader: vk::ShaderModule,

    viewports: [vk::Viewport; 1],
    scissors: [vk::Rect2D; 1],

    pipeline_layout: vk::PipelineLayout,
    pipeline: vk::Pipeline
}

impl Pipeline {
    fn new(device: Device, renderpass: vk::RenderPass, resolution: vk::Extent2D, vertex_shader_path: &str, fragment_shader_path: &str) -> Self {
        let vertex_shader = unsafe {
            device.create_shader_module(
                &vk::ShaderModuleCreateInfo::builder()
                    .code(&read_spv(&mut std::fs::File::open(vertex_shader_path).expect("Unable to open vertex shader")).expect("Failed to read vertex shader")),
                None
            ).expect("Unable to create vertex shader module")
        };
        let fragment_shader = unsafe {
            device.create_shader_module(
                &vk::ShaderModuleCreateInfo::builder()
                    .code(&read_spv(&mut std::fs::File::open(fragment_shader_path).expect("Unable to open fragment shader")).expect("Failed to read fragment shader")),
                None
            ).expect("Unable to create fragment shader module")
        };

        let pipeline_layout = unsafe { device.create_pipeline_layout(
            &vk::PipelineLayoutCreateInfo::builder()
                .push_constant_ranges(&[
                    vk::PushConstantRange {
                        stage_flags: vk::ShaderStageFlags::FRAGMENT | vk::ShaderStageFlags::VERTEX,
                        offset: 0,
                        size: std::mem::size_of::<PushConstants>() as _
                    }
                ]),
            None
        ) }.expect("Unable to create pipeline layout");

        let entry_point = b"main\0".as_ptr() as _;
        let shader_stages = [
            vk::PipelineShaderStageCreateInfo {
                module: vertex_shader,
                p_name: entry_point,
                stage: vk::ShaderStageFlags::VERTEX,
                ..Default::default()
            },
            vk::PipelineShaderStageCreateInfo {
                module: fragment_shader,
                p_name: entry_point,
                stage: vk::ShaderStageFlags::FRAGMENT,
                ..Default::default()
            }
        ];

        let viewports = [vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: resolution.width as _,
            height: resolution.height as _,
            min_depth: 0.0,
            max_depth: 1.0
        }];

        let scissors = [vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: resolution
        }];

        let pipelines = unsafe { device.create_graphics_pipelines(
            vk::PipelineCache::null(),
            &[vk::GraphicsPipelineCreateInfo::builder()
                .stages(&shader_stages)
                .vertex_input_state(&vk::PipelineVertexInputStateCreateInfo::builder()
                    .vertex_attribute_descriptions(&[vk::VertexInputAttributeDescription {
                        location: 0,
                        binding: 0,
                        format: vk::Format::R32G32_SFLOAT,
                        offset: 0
                    }])
                    .vertex_binding_descriptions(&[vk::VertexInputBindingDescription {
                        binding: 0,
                        stride: std::mem::size_of::<[f32; 2]>() as _,
                        input_rate: vk::VertexInputRate::VERTEX
                    }])
                )
                .input_assembly_state(&vk::PipelineInputAssemblyStateCreateInfo {
                    topology: vk::PrimitiveTopology::TRIANGLE_STRIP,
                    ..Default::default()
                })
                .viewport_state(&vk::PipelineViewportStateCreateInfo::builder()
                    .viewports(&viewports)
                    .scissors(&scissors)
                )
                .rasterization_state(&vk::PipelineRasterizationStateCreateInfo {
                    front_face: vk::FrontFace::COUNTER_CLOCKWISE,
                    line_width: 1.0,
                    polygon_mode: vk::PolygonMode::FILL,
                    ..Default::default()
                })
                .multisample_state(&vk::PipelineMultisampleStateCreateInfo {
                    rasterization_samples: vk::SampleCountFlags::TYPE_1,
                    ..Default::default()
                })
                .depth_stencil_state({
                    let noop_stencil = vk::StencilOpState {
                        fail_op: vk::StencilOp::KEEP,
                        pass_op: vk::StencilOp::KEEP,
                        depth_fail_op: vk::StencilOp::KEEP,
                        compare_op: vk::CompareOp::ALWAYS,
                        ..Default::default()
                    };
                    &vk::PipelineDepthStencilStateCreateInfo {
                        depth_test_enable: 0,
                        depth_write_enable: 0,
                        depth_compare_op: vk::CompareOp::LESS_OR_EQUAL,
                        front: noop_stencil,
                        back: noop_stencil,
                        max_depth_bounds: 1.0,
                        ..Default::default()
                    }
                })
                .color_blend_state(&vk::PipelineColorBlendStateCreateInfo::builder()
                    .logic_op(vk::LogicOp::CLEAR)
                    .attachments(&[vk::PipelineColorBlendAttachmentState {
                        blend_enable: 0,
                        src_color_blend_factor: vk::BlendFactor::SRC_COLOR,
                        dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_DST_COLOR,
                        color_blend_op: vk::BlendOp::ADD,
                        src_alpha_blend_factor: vk::BlendFactor::ZERO,
                        dst_alpha_blend_factor: vk::BlendFactor::ZERO,
                        alpha_blend_op: vk::BlendOp::ADD,
                        color_write_mask: vk::ColorComponentFlags::all()
                    }])
                )
                .dynamic_state(&vk::PipelineDynamicStateCreateInfo::builder()
                    .dynamic_states(&[vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR])
                )
                .layout(pipeline_layout)
                .render_pass(renderpass)
                .build()
            ],
            None
        ) }.expect("Unable to construct graphics pipeline");

        let pipeline = {
            for pipeline in pipelines[1..].iter() {
                eprintln!("Got an extra pipeline!");
                unsafe { device.destroy_pipeline(*pipeline, None) };
            }
            pipelines[0]
        };

        Self {
            device,
            vertex_shader,
            fragment_shader,

            viewports,
            scissors,

            pipeline_layout,
            pipeline
        }
    }
}
impl std::ops::Deref for Pipeline {
    type Target = vk::Pipeline;
    fn deref(&self) -> &Self::Target {
        &self.pipeline
    }
}
impl std::ops::Drop for Pipeline {
    fn drop(&mut self) {
        unsafe {
            self.device.destroy_pipeline(self.pipeline, None);
            self.device.destroy_shader_module(self.vertex_shader, None);
            self.device.destroy_shader_module(self.fragment_shader, None);
            self.device.destroy_pipeline_layout(self.pipeline_layout, None);
        }
    }
}

fn surface_name() -> *const i8 {
    #[cfg(all(target_family = "unix", not(target_os = "macos")))]
    {
        ash::extensions::khr::XcbSurface::name().as_ptr()
    }
    #[cfg(target_os = "windows")]
    {
        ash::extensions::khr::Win32Surface::name().as_ptr()
    }
    // If there is an error here it is because you are using an unsuported platform
}

#[cfg(all(target_family = "unix", not(target_os = "macos")))]
fn surface_setup(windowing: &windowing::Platform, entry: &Entry, instance: &Instance, surface_loader: &Surface) -> (vk::PhysicalDevice, u32, vk::SurfaceKHR) {
    use ash::extensions::khr::XcbSurface;
    let (connection, window, visual) = windowing.handles();
    let xcb_info = vk::XcbSurfaceCreateInfoKHR {
        connection: connection as _,
        window: *window,
        ..Default::default()
    };

    let xcb_surface_loader = XcbSurface::new(entry, instance);
    let xcb_surface = unsafe { xcb_surface_loader.create_xcb_surface(&xcb_info, None) }.unwrap();

    let physical_devices = unsafe { instance.enumerate_physical_devices() }.unwrap();
    let (physical_device, queue_family_index) = unsafe { physical_devices.iter().map(|device| {
        instance.get_physical_device_queue_family_properties(*device).iter().enumerate().filter_map(|(index, ref info)| {
            let index = index as u32;
            let supported = info.queue_flags.contains(vk::QueueFlags::GRAPHICS) && xcb_surface_loader.get_physical_device_xcb_presentation_support(
                *device,
                index,
                (&mut *(connection as *mut std::ffi::c_void)) as _,
                *visual
            ) && surface_loader.get_physical_device_surface_support(*device, index, xcb_surface).expect("Unable to get physical device surface support");
            if supported {
                Some((*device, index))
            } else {
                None
            }
        }).next()
    })}.filter_map(|d| d).next().expect("Unable to find suitable graphics device");

    (physical_device, queue_family_index, xcb_surface)
}

#[cfg(target_os = "windows")]
fn surface_setup(windowing: &windowing::Platform, entry: &Entry, instance: &Instance, surface_loader: &Surface) -> (vk::PhysicalDevice, u32, vk::SurfaceKHR) {
    use ash::extensions::khr::Win32Surface;
    let (connection, window) = windowing.handles();
    let win32_info = vk::Win32SurfaceCreateInfoKHR {
        hinstance: connection as _,
        hwnd: window as _,
        ..Default::default()
    };

    let win32_surface_loader = Win32Surface::new(entry, instance);
    let win32_surface = unsafe { win32_surface_loader.create_win32_surface(&win32_info, None).unwrap() };

    let physical_devices = unsafe { instance.enumerate_physical_devices() }.unwrap();
    let (physical_device, queue_family_index) = unsafe { physical_devices.iter().map(|device| {
        instance.get_physical_device_queue_family_properties(*device).iter().enumerate().filter_map(|(index, ref info)| {
            let index = index as u32;
            let supported = info.queue_flags.contains(vk::QueueFlags::GRAPHICS) && win32_surface_loader.get_physical_device_win32_presentation_support(
                *device,
                index,
            ) && surface_loader.get_physical_device_surface_support(*device, index, win32_surface).expect("Unable to get physical device surface support");
            if supported {
                Some((*device, index))
            } else {
                None
            }
        }).next()
    })}.filter_map(|d| d).next().expect("Unable to find suitable graphics device");

    (physical_device, queue_family_index, win32_surface)
}

pub struct Vulkan {
    entry: Entry,
    instance: Instance,
    physical_device: vk::PhysicalDevice,
    device: Device,
    memory_properties: vk::PhysicalDeviceMemoryProperties,

    surface: vk::SurfaceKHR,
    surface_loader: Surface,
    surface_format: vk::SurfaceFormatKHR,
    surface_capabilities: vk::SurfaceCapabilitiesKHR,
    image_count: u32,
    present_mode: vk::PresentModeKHR,

    pool: vk::CommandPool,
    render_command_buffer: vk::CommandBuffer,
    render_queue: vk::Queue,
    present_semaphore: vk::Semaphore,
    render_semaphore: vk::Semaphore,

    swapchain: vk::SwapchainKHR,
    swapchain_loader: Swapchain,
    swapchain_images: Vec<vk::Image>,
    swapchain_image_views: Vec<vk::ImageView>,

    vertex_buffer: vk::Buffer,
    vertex_memory: vk::DeviceMemory,
    pipeline: Option<Pipeline>,

    pub resolution: vk::Extent2D,
    renderpass: vk::RenderPass,
    framebuffers: Vec<vk::Framebuffer>
}
impl Vulkan {
    pub fn new(windowing: &windowing::Platform) -> Self {
        let entry = Entry::new().unwrap();
        let app_info = vk::ApplicationInfo {
            api_version: vk::make_version(1, 0, 0),
            ..Default::default()
        };
        let extensions = &[
            Surface::name().as_ptr(),
            surface_name()
        ];
        let create_info = vk::InstanceCreateInfo {
            p_application_info: &app_info,
            enabled_extension_count: extensions.len() as u32,
            pp_enabled_extension_names: extensions as _,
            ..Default::default()
        };
        let instance = unsafe { entry.create_instance(&create_info, None).unwrap() };

        let surface_loader = Surface::new(&entry, &instance);

        let (physical_device, queue_family_index, surface) = surface_setup(windowing, &entry, &instance, &surface_loader);

        let device = unsafe { instance.create_device(
            physical_device,
            &vk::DeviceCreateInfo::builder()
                .queue_create_infos(&[ vk::DeviceQueueCreateInfo::builder().queue_family_index(queue_family_index).queue_priorities(&[1.0]).build() ])
                .enabled_extension_names(&[Swapchain::name().as_ptr()])
                .enabled_features(&vk::PhysicalDeviceFeatures { ..Default::default() }),
            None
        ) }.expect("Unable to create Vulkan device");
        let memory_properties = unsafe { instance.get_physical_device_memory_properties(physical_device) };
        let render_queue = unsafe { device.get_device_queue(queue_family_index, 0) };
        let surface_format = unsafe { surface_loader.get_physical_device_surface_formats(physical_device, surface) }
            .expect("Unable to get supported surface formats")
            .iter()
            .map(|format| match format.format {
                vk::Format::UNDEFINED => vk::SurfaceFormatKHR {
                    format: vk::Format::R8G8B8_UNORM,
                    color_space: format.color_space,
                },
                _ => *format
            })
            .next()
            .expect("No suitable surface formats");

        let surface_capabilities = unsafe { surface_loader.get_physical_device_surface_capabilities(physical_device, surface) }.expect("Unable to get surface capabilities");
        let image_count = if surface_capabilities.max_image_count > 0 && surface_capabilities.min_image_count < surface_capabilities.max_image_count {
            surface_capabilities.max_image_count
        } else {
            surface_capabilities.min_image_count + 1
        };

        let resolution = if let std::u32::MAX = surface_capabilities.current_extent.width {
            let size = windowing.size().expect("Unable to get window geometry");
            vk::Extent2D {
                width: size.width as u32,
                height: size.height as u32
            }
        } else {
            surface_capabilities.current_extent
        };

        let present_mode = unsafe { surface_loader
            .get_physical_device_surface_present_modes(physical_device, surface) }.expect("Unable to get surface presentation modes")
            .iter()
            .cloned()
            .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
            .unwrap_or(vk::PresentModeKHR::FIFO);

        let swapchain_loader = Swapchain::new(&instance, &device);
        let swapchain = unsafe { swapchain_loader.create_swapchain(
            &vk::SwapchainCreateInfoKHR::builder()
                .surface(surface)
                .min_image_count(image_count)
                .image_color_space(surface_format.color_space)
                .image_format(surface_format.format)
                .image_extent(resolution)
                .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
                .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
                .pre_transform(if surface_capabilities.supported_transforms.contains(vk::SurfaceTransformFlagsKHR::IDENTITY) { vk::SurfaceTransformFlagsKHR::IDENTITY } else { surface_capabilities.current_transform})
                .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
                .present_mode(present_mode)
                .clipped(true)
                .image_array_layers(1),
            None
        ) }.expect("Unable to create swapchain");

        let pool = unsafe { device.create_command_pool(
            &vk::CommandPoolCreateInfo::builder()
                .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
                .queue_family_index(queue_family_index),
            None
        ) }.expect("Unable to create command pool");

        let command_buffers = unsafe { device.allocate_command_buffers(
            &vk::CommandBufferAllocateInfo::builder()
                .command_buffer_count(2)
                .command_pool(pool)
                .level(vk::CommandBufferLevel::PRIMARY)
        ) }.expect("Unable to allocate command buffers");
        let render_command_buffer = command_buffers[0];
        
        let swapchain_images = unsafe { swapchain_loader.get_swapchain_images(swapchain).expect("Unable to get swapchain images") };
        let swapchain_image_views: Vec<_> = swapchain_images.iter().map(|&image| {
            unsafe { device.create_image_view(
                &vk::ImageViewCreateInfo::builder()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface_format.format)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::R,
                        g: vk::ComponentSwizzle::G,
                        b: vk::ComponentSwizzle::B,
                        a: vk::ComponentSwizzle::A
                    })
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1
                    })
                    .image(image),
                None
            ).expect("Unable to get image view") }
        }).collect();

        let semaphore_create_info = vk::SemaphoreCreateInfo::default();
        let present_semaphore = unsafe { device.create_semaphore(&semaphore_create_info, None) }.unwrap();
        let render_semaphore = unsafe { device.create_semaphore(&semaphore_create_info, None) }.unwrap();

        let renderpass = unsafe { device.create_render_pass(
            &vk::RenderPassCreateInfo::builder()
                .attachments(&[
                    vk::AttachmentDescription {
                        format: surface_format.format,
                        samples: vk::SampleCountFlags::TYPE_1,
                        load_op: vk::AttachmentLoadOp::CLEAR,
                        store_op: vk::AttachmentStoreOp::STORE,
                        final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                        ..Default::default()
                    }
                ])
                .subpasses(&[
                    vk::SubpassDescription::builder()
                        .color_attachments(&[vk::AttachmentReference {
                            attachment: 0,
                            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL
                        }])
                        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
                        .build()
                ])
                .dependencies(&[
                    vk::SubpassDependency::builder()
                        .src_subpass(vk::SUBPASS_EXTERNAL)
                        .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                        .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                        .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                        .build()
                ]),
            None
        ) }.expect("Unable to create renderpass");

        let framebuffers: Vec<_> = swapchain_image_views.iter().map(|&image_view| {
            unsafe { device.create_framebuffer(
                &vk::FramebufferCreateInfo::builder()
                    .render_pass(renderpass)
                    .attachments(&[image_view])
                    .width(resolution.width)
                    .height(resolution.height)
                    .layers(1),
                None
            ) }.expect("Unable to create framebuffer image")
        }).collect();

        let vertex_buffer = unsafe { device.create_buffer(
            &vk::BufferCreateInfo {
                size: (VERTICES.len() * std::mem::size_of::<[f32; 2]>()) as _,
                usage: vk::BufferUsageFlags::VERTEX_BUFFER,
                sharing_mode: vk::SharingMode::EXCLUSIVE,
                ..Default::default()
            },
            None
        ) }.expect("Unable to create vertex buffer");

        let vertex_memory = unsafe {
            let memory_requirements = device.get_buffer_memory_requirements(vertex_buffer);
            let memory_type_index = find_memory_type(memory_requirements, memory_properties, vk::MemoryPropertyFlags::HOST_COHERENT | vk::MemoryPropertyFlags::HOST_VISIBLE);
            let memory = device.allocate_memory(
                &vk::MemoryAllocateInfo {
                    allocation_size: memory_requirements.size,
                    memory_type_index,
                    ..Default::default()
                },
                None
            ).expect("Unable to allocate memory for vertex buffer");

            let pointer = device.map_memory(memory, 0, memory_requirements.size, vk::MemoryMapFlags::empty()).expect("Unable to map vertex buffer to host memory");
            let mut align = ash::util::Align::new(pointer, std::mem::align_of::<[f32; 2]>() as u64, memory_requirements.size);
            align.copy_from_slice(&VERTICES);
            device.unmap_memory(memory);
            device.bind_buffer_memory(vertex_buffer, memory, 0).expect("Unable to bind vertex buffer to GPU memory");

            memory
        };

        let pipeline = Some(Pipeline::new(device.clone(), renderpass, resolution, "shader/vert.spv", "shader/frag.spv"));

        Self {
            entry,
            instance,
            physical_device,
            device,
            memory_properties,

            surface,
            surface_loader,
            surface_format,
            surface_capabilities,
            image_count,
            present_mode,

            pool,
            render_command_buffer,
            render_queue,
            present_semaphore,
            render_semaphore,

            swapchain,
            swapchain_loader,
            swapchain_images,
            swapchain_image_views,

            vertex_buffer,
            vertex_memory,
            pipeline,

            resolution,
            renderpass,
            framebuffers,
        }
    }

    pub fn redraw(&mut self, epoch: std::time::Instant, mouse: [f32; 2]) -> bool {
        unsafe {
            // Acquire next framebuffer image
            let (framebuffer_index, _) = self.swapchain_loader.acquire_next_image(self.swapchain, std::u64::MAX, self.present_semaphore, vk::Fence::null()).expect("Unable to start rendering to next swapchain");

            self.device.reset_command_buffer(self.render_command_buffer, vk::CommandBufferResetFlags::RELEASE_RESOURCES).expect("Unable to reset command buffer");
            self.device.begin_command_buffer(
                self.render_command_buffer,
                &vk::CommandBufferBeginInfo::builder()
                    .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)
            ).expect("Unable to begin command buffer");

            // Draw
            self.device.cmd_begin_render_pass(
                self.render_command_buffer,
                &vk::RenderPassBeginInfo::builder()
                    .render_pass(self.renderpass)
                    .framebuffer(self.framebuffers[framebuffer_index as usize])
                    .render_area(vk::Rect2D {
                        offset: vk::Offset2D { x: 0, y: 0 },
                        extent: self.resolution
                    })
                    .clear_values(&[
                        vk::ClearValue {
                            color: vk::ClearColorValue {
                                float32: [0.0, 0.0, 0.0, 0.0]
                            }
                        }
                    ]),
                vk::SubpassContents::INLINE    
            );

            let push = PushConstants::new((std::time::Instant::now() - epoch).as_secs_f32(), self.resolution, mouse);
            self.device.cmd_push_constants(self.render_command_buffer, self.pipeline.as_ref().unwrap().pipeline_layout.clone(), vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT, 0, push.as_bytes());

            self.device.cmd_bind_pipeline(self.render_command_buffer, vk::PipelineBindPoint::GRAPHICS, self.pipeline.as_ref().unwrap().pipeline.clone());
            self.device.cmd_set_viewport(self.render_command_buffer, 0, &self.pipeline.as_ref().unwrap().viewports);
            self.device.cmd_set_scissor(self.render_command_buffer, 0, &self.pipeline.as_ref().unwrap().scissors);
            self.device.cmd_bind_vertex_buffers(self.render_command_buffer, 0, &[self.vertex_buffer], &[0]);
            self.device.cmd_draw(self.render_command_buffer, 4, 1, 0, 0);

            self.device.cmd_end_render_pass(self.render_command_buffer);

            self.device.end_command_buffer(self.render_command_buffer).expect("Unable to end command buffer");

            let fence = self.device.create_fence(&vk::FenceCreateInfo::default(), None).expect("Unable to create fence");
            self.device.queue_submit(self.render_queue, &[
                vk::SubmitInfo::builder()
                    .wait_semaphores(&[self.present_semaphore])
                    .wait_dst_stage_mask(&[])
                    .command_buffers(&[self.render_command_buffer])
                    .signal_semaphores(&[self.render_semaphore])
                    .build()
            ], fence).expect("Unable to submit command buffer");

            self.device.wait_for_fences(&[fence], true, std::u64::MAX).expect("Failed to wait for fence");
            self.device.destroy_fence(fence, None);

            
            match self.swapchain_loader.queue_present(
                self.render_queue,
                &vk::PresentInfoKHR::builder()
                    .wait_semaphores(&[self.render_semaphore])
                    .swapchains(&[self.swapchain])
                    .image_indices(&[framebuffer_index])
            ) {
                Ok(suboptimal) => suboptimal,
                Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => true,
                error => error.expect("Unable to present swapchain to the screen")
            }
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        // Clean up old swapchain
        unsafe {
            self.pipeline = None;
            self.device.device_wait_idle().unwrap();
            self.device.destroy_render_pass(self.renderpass, None);
            for &framebuffer in self.framebuffers.iter() {
                self.device.destroy_framebuffer(framebuffer, None);
            }
            for &image_view in self.swapchain_image_views.iter() {
                self.device.destroy_image_view(image_view, None);
            }
            self.swapchain_loader.destroy_swapchain(self.swapchain, None);
        }


        // Create new swapchain
        self.resolution = vk::Extent2D {
            width,
            height
        };

        self.swapchain = unsafe { self.swapchain_loader.create_swapchain(
            &vk::SwapchainCreateInfoKHR::builder()
                .surface(self.surface)
                .min_image_count(self.image_count)
                .image_color_space(self.surface_format.color_space)
                .image_format(self.surface_format.format)
                .image_extent(self.resolution)
                .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
                .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
                .pre_transform(if self.surface_capabilities.supported_transforms.contains(vk::SurfaceTransformFlagsKHR::IDENTITY) { vk::SurfaceTransformFlagsKHR::IDENTITY } else { self.surface_capabilities.current_transform})
                .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
                .present_mode(self.present_mode)
                .clipped(true)
                .image_array_layers(1),
            None
        ) }.expect("Unable to create swapchain");
        
        self.swapchain_images = unsafe { self.swapchain_loader.get_swapchain_images(self.swapchain).expect("Unable to get swapchain images") };
        self.swapchain_image_views = self.swapchain_images.iter().map(|&image| {
            unsafe { self.device.create_image_view(
                &vk::ImageViewCreateInfo::builder()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(self.surface_format.format)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::R,
                        g: vk::ComponentSwizzle::G,
                        b: vk::ComponentSwizzle::B,
                        a: vk::ComponentSwizzle::A
                    })
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1
                    })
                    .image(image),
                None
            ).expect("Unable to get image view") }
        }).collect();

        self.renderpass = unsafe { self.device.create_render_pass(
            &vk::RenderPassCreateInfo::builder()
                .attachments(&[
                    vk::AttachmentDescription {
                        format: self.surface_format.format,
                        samples: vk::SampleCountFlags::TYPE_1,
                        load_op: vk::AttachmentLoadOp::CLEAR,
                        store_op: vk::AttachmentStoreOp::STORE,
                        final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                        ..Default::default()
                    }
                ])
                .subpasses(&[
                    vk::SubpassDescription::builder()
                        .color_attachments(&[vk::AttachmentReference {
                            attachment: 0,
                            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL
                        }])
                        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
                        .build()
                ])
                .dependencies(&[
                    vk::SubpassDependency::builder()
                        .src_subpass(vk::SUBPASS_EXTERNAL)
                        .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                        .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
                        .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                        .build()
                ]),
            None
        ) }.expect("Unable to create renderpass");

        self.framebuffers = self.swapchain_image_views.iter().map(|&image_view| {
            unsafe { self.device.create_framebuffer(
                &vk::FramebufferCreateInfo::builder()
                    .render_pass(self.renderpass)
                    .attachments(&[image_view])
                    .width(self.resolution.width)
                    .height(self.resolution.height)
                    .layers(1),
                None
            ) }.expect("Unable to create framebuffer image")
        }).collect();

        self.pipeline = Some(Pipeline::new(self.device.clone(), self.renderpass, self.resolution, "shader/vert.spv", "shader/frag.spv"));
    }

    pub fn ready(&self, size: &Size) -> bool {
        self.resolution.width == size.width as _ && self.resolution.height == size.height as _
    }
}

impl std::ops::Drop for Vulkan {
    fn drop(&mut self) {
        unsafe {
            self.device.device_wait_idle().unwrap();
            self.device.free_memory(self.vertex_memory, None);
            self.device.destroy_buffer(self.vertex_buffer, None);
            self.device.destroy_render_pass(self.renderpass, None);
            for &framebuffer in self.framebuffers.iter() {
                self.device.destroy_framebuffer(framebuffer, None);
            }
            for &image_view in self.swapchain_image_views.iter() {
                self.device.destroy_image_view(image_view, None);
            }
            self.device.destroy_semaphore(self.present_semaphore, None);
            self.device.destroy_semaphore(self.render_semaphore, None);
            self.device.free_command_buffers(self.pool, &[self.render_command_buffer]);
            self.device.destroy_command_pool(self.pool, None);
            self.swapchain_loader.destroy_swapchain(self.swapchain, None);
            self.device.destroy_device(None);
            self.surface_loader.destroy_surface(self.surface, None);
            self.instance.destroy_instance(None);
        }
    }
}
