use crate::prelude::*;
use crate::ffi::*;
use crate::c_enum;

pub struct Xcb {
    connection: *mut Connection,
    window: Window,
    visual: Visual,
    close_event: Atom
}

impl Xcb {
    pub fn new() -> Result<Self> {
        unsafe {

            let connection = xcb_connect(null(), null());
            let screen = xcb_setup_roots_iterator(xcb_get_setup(connection)).data;
    
            let window = Window::generate_id(connection);
            xcb_create_window(
                connection,
                COPY_FROM_PARENT,
                window,
                (*screen).root,
                0, 0, 600, 200,
                10,
                Class::InputOutput,
                (*screen).root_visual,
                ValueMask::None | ValueMask::Event, &[EventMask::Expose | EventMask::None] as _
            ).check(connection)?;
            let visual = (*screen).root_visual;

            // Request intern atoms
            let window_name = intern_atom(connection, "_NET_WM_NAME");
            let window_type = intern_atom(connection, "_NET_WM_WINDOW_TYPE");
            let window_type_desktop = intern_atom(connection, "_NET_WM_WINDOW_TYPE_DESKTOP");
            let desktops = intern_atom(connection, "_NET_WM_DESKTOP");
            let delete_window = intern_atom(connection, "WM_DELETE_WINDOW");
            let protocols = intern_atom(connection, "WM_PROTOCOLS");

            // Await intern atoms
            let window_name = window_name.check(connection)?;
            let window_type = window_type.check(connection)?;
            let window_type_desktop = window_type_desktop.check(connection)?;
            let desktops = desktops.check(connection)?;
            let delete_window = delete_window.check(connection)?;
            let protocols = protocols.check(connection)?;

            // Set properties
            replace_property(connection, window, window_name, b"Skypaper")?;
            replace_property(connection, window, window_type, &[window_type_desktop])?;
            replace_property(connection, window, desktops, &[std::u32::MAX])?;
            replace_property(connection, window, protocols, &[delete_window])?;

            xcb_map_window(connection, window);

            Ok(Self {
                connection,
                window,
                visual,
                close_event: delete_window
            })
        }
    }

    pub fn size(&self) -> Result<Size> {
        Ok(unsafe {
            let mut error = &mut XcbError::none() as *mut _;
            let geometry = xcb_get_geometry_reply(
                self.connection,
                xcb_get_geometry(self.connection, self.window),
                &mut error
            );
            if !error.is_null() && (*error).error_code != 0 {
                return Err(Error((*error).error_code))
            }
            let GeometryReply { x, y, width, height, ..} = *geometry;
            free(geometry as _);

            Size {
                x, y, width, height
            } 
        })
    }

    pub fn mouse_position(&self) -> Result<MousePosition> {
        Ok(unsafe {
            let mut error = &mut XcbError::none() as *mut _;
            let pointer = xcb_query_pointer_reply(
                self.connection,
                xcb_query_pointer(self.connection, self.window),
                &mut error
            );
            if !error.is_null() && (*error).error_code != 0 {
                return Err(Error((*error).error_code))
            }
            let PointerReply { root_x, root_y, ..} = *pointer;
            free(pointer as _);

            MousePosition {
                x: root_x as _, y: root_y as _
            } 
        })
    }

    pub fn handles(&self) -> (*mut Connection, Window, Visual) {
        (self.connection, self.window, self.visual)
    }

    pub fn event_pump(&mut self) -> Option<super::Event> {
        let any_event = unsafe { xcb_poll_for_event(self.connection) }; 
        if any_event.is_null() {
            None
        } else {
            if let Some(event) = unsafe { Event::new(any_event) } {
                match event {
                    Event::Expose { size: Size { width, height, ..}, ..} => {
                        Some(super::Event::Resize(width as u32, height as u32))
                    },
                    Event::ClientMessage { data, ..} if data.atoms()[0] == *self.close_event => Some(super::Event::Quit),
                    _ => panic!("Unknown event received!")
                }
            } else {
                None
            }
        }
    }
}

extern "C" {
    /// The same C allocator MUST be used to free xcb-alloced memory
    fn free(pointer: *mut Void);
}

#[link(name = "xcb")]
extern "C" {
    /// Start a connection to the X server
    /// Return value must be freed with `xcb_disconnect`
    fn xcb_connect(display: *const u8, screen: *const i32) -> *mut Connection;
    fn xcb_disconnect(connection: *mut Connection);

    fn xcb_generate_id(connection: *mut Connection) -> u32;
    /// It is unclear whether we must free the return value
    fn xcb_request_check(connection: *mut Connection, cookie: Cookie) -> *const XcbError;
    fn xcb_flush(connection: *mut Connection) -> i32;

    /// Do not free the return value
    fn xcb_get_setup(connection: *mut Connection) -> *const Setup;
    fn xcb_setup_roots_iterator<'a>(setup: *const Setup) -> ScreenIterator;

    fn xcb_create_window(
        connection: *mut Connection,
        depth: u8,
        window: Window,
        parent: Window,
        x: u16,
        y: u16,
        width: u16,
        height: u16,
        border_width: u16,
        class: Class,
        visual: Visual,
        value_mask: u32,
        value_list: *const u32
    ) -> Cookie;
    fn xcb_map_window(connection: *mut Connection, window: Window) -> Cookie;

    fn xcb_get_geometry(connection: *mut Connection, window: Window) -> GeometryCookie;
    fn xcb_get_geometry_reply(connection: *mut Connection, cookie: GeometryCookie, error: *mut *mut XcbError) -> *mut GeometryReply;
    fn xcb_query_pointer(connection: *mut Connection, window: Window) -> PointerCookie;
    fn xcb_query_pointer_reply(connection: *mut Connection, cookie: PointerCookie, error: *mut *mut XcbError) -> *mut PointerReply;

    fn xcb_wait_for_event(connection: *mut Connection) -> *mut event::Any;
    fn xcb_poll_for_event(connection: *mut Connection) -> *mut event::Any; 

    fn xcb_change_property(connection: *mut Connection, mode: PropertyMode, window: Window, property: Atom, r#type: Atom, format: u8, data_len: u32, data: *const Void) -> Cookie;
    fn xcb_intern_atom(connection: *mut Connection, only_if_exists: u8, name_len: u16, name: *const u8) -> InternAtomCookie;
    /// The return value must be freed
    fn xcb_intern_atom_reply(connection: *mut Connection, cookie: InternAtomCookie, error: *mut *mut XcbError) -> *mut InternAtomReply;
}

#[inline(always)]
fn intern_atom(connection: *mut Connection, name: &str) -> InternAtomCookie {
    unsafe { xcb_intern_atom(connection, false as u8, name.len() as u16, name.as_ptr()) }
}

#[inline(always)]
unsafe fn replace_property<T: AtomType>(connection: *mut Connection, window: Window, property: Atom, data: &[T]) -> Result<()> {
    xcb_change_property(connection, PropertyMode::Replace, window, property, T::atom(), std::mem::size_of::<T>() as u8 * 8, data.len() as u32, data.as_ptr() as _).check(connection)
}

#[repr(C)]
pub struct Connection { _opaque: [u8; 0] }
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct Window { _id: u32 }
impl Window {
    pub fn generate_id(connection: *mut Connection) -> Self {
        Self {
            _id: unsafe { xcb_generate_id(connection) }
        }
    }
}
impl std::ops::Deref for Window {
    type Target = u32;
    fn deref(&self) -> &Self::Target {
        &self._id
    }
}
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct Visual { _id: u32 }
impl std::ops::Deref for Visual {
    type Target = u32;
    fn deref(&self) -> &Self::Target {
        &self._id
    }
}

#[repr(C)]
struct Cookie { _opaque: [u8; 0] }
impl Cookie {
    fn check(self, connection: *mut Connection) -> std::result::Result<(), Error> {
        unsafe {
            let error = xcb_request_check(connection, self);
            if error.is_null() {
                Ok(())
            } else {
                Err(Error((*error).error_code))
            }
        }
    }
}


#[repr(C)]
struct InternAtomReply {
    response_type: u8,
    _pad: u8,
    sequence: u16,
    length: u32,
    atom: Atom
}

#[derive(Copy, Clone)]
#[repr(C)]
struct InternAtomCookie { _sequence: u32 }
impl InternAtomCookie {
    fn check(&self, connection: *mut Connection) -> Result<Atom> {
        unsafe {
            let mut error = &mut XcbError::none() as *mut _;
            let response = xcb_intern_atom_reply(connection, *self, &mut error);
            if !error.is_null() && (*error).error_code != 0 {
                Err(Error((*error).error_code))
            } else {
                let atom = (*response).atom;
                free(response as _);
                Ok(atom)
            }
        }
    }
}

#[repr(C)]
struct Colourmap { _opaque: u32 }
#[repr(C)]
struct Keycode { _opaque: u8 }
type Timestamp = u32;

#[repr(C)]
struct Screen {
    root: Window,
    default_colourmap: Colourmap,
    white: u32,
    black: u32,
    input_masks: u32,
    width: u16,
    height: u16,
    width_millimeters: u16,
    height_millimeters: u16,
    min_installed_maps: u16,
    max_installed_maps: u16,
    root_visual: Visual,
    backing_stores: u8,
    save_unders: u8,
    root_depth: u8,
    allowed_depths_len: u8 
}

#[repr(C)]
struct Setup {
    status: u8,
    _pad0: u8,
    major_version: u16,
    minor_version: u16,
    length: u16,
    release: u32,
    resource_id_base: u32,
    resource_id_mask: u32,
    motion_buffer_size: u32,
    vendor_len: u16,
    max_request_length: u16,
    roots_len: u8,
    pixmap_formats_len: u8,
    image_byte_order: u8,
    bitmap_format_bit_order: u8,
    bitmap_format_scanline_unit: u8,
    bitmap_format_scanline_pad: u8,
    min_keycode: Keycode,
    max_keycode: Keycode,
    _pad1: [u8; 4]
}

#[repr(C)]
struct ScreenIterator {
    data: *const Screen,
    rem: i32,
    index: i32
}

#[repr(C)]
struct GeometryCookie {
    sequence: u32
}

#[repr(C)]
struct GeometryReply {
    response_type: u8,
    depth: u8,
    sequence: u16,
    length: u32,
    root: Window,
    x: u16,
    y: u16,
    width: u16,
    height: u16,
    border_width: u16,
    _pad: [u8; 2]
}

#[repr(C)]
struct PointerCookie {
    sequence: u32
}

#[repr(C)]
struct PointerReply {
    response_type: u8,
    same_screen: u8,
    sequence: u16,
    length: u32,
    root: Window,
    child: Window,
    root_x: i16,
    root_y: i16,
    x: i16,
    y: i16,
    mask: u16,
    _pad: [u8; 2]
}

#[repr(C)]
struct XcbError {
    response_type: u8,
    error_code: u8,
    sequence: u16,
    resource_id: u32,
    minor_code: u16,
    major_code: u8,
    _pad: [u8; 21],
    full_sequence: u32
}

impl XcbError {
    fn none() -> Self {
        Self {
            response_type: 0,
            error_code: 0,
            sequence: 0,
            resource_id: 0,
            minor_code: 0,
            major_code: 0,
            _pad: [0; 21],
            full_sequence: 0
        }
    }
}

// Consts
const COPY_FROM_PARENT: u8 = 0;

c_enum! { Class -> u16 = [InputOutput: 0] }
c_enum! { Atom -> u32 = [WM_NAME: 39] }
c_enum! { PropertyMode -> u8 = [Replace: 0] }
c_enum! { ValueMask -> u32 = [None: 0, Event: 1 << 11]}
impl std::ops::BitOr for ValueMask {
    type Output = u32;
    fn bitor(self, rhs: Self) -> u32 {
        self.0 | rhs.0
    }
}
impl std::ops::BitOr<ValueMask> for u32 {
    type Output = u32;
    fn bitor(self, rhs: ValueMask) -> u32 {
        self | rhs.0
    }
}

c_enum! { EventMask -> u32 = [
    None: 0,
    KeyPress: 1,
    KeyRelease: 1 << 1,
    ButtonPress: 1 << 2,
    Buttonrelease: 1 << 3,
    EnterWindow: 1 << 4,
    LeaveWindow: 1 << 5,
    MouseMotion: 1 << 6,
    MouseMotionHint: 1 << 7,

    Expose: 1 << 15,
    Visibility: 1 << 16,

    Resize: 1 << 18,
    Notify: 1 << 19
] }
impl std::ops::BitOr for EventMask {
    type Output = u32;
    fn bitor(self, rhs: Self) -> u32 {
        self.0 | rhs.0
    }
}
impl std::ops::BitOr<EventMask> for u32 {
    type Output = u32;
    fn bitor(self, rhs: EventMask) -> u32 {
        self | rhs.0
    }
}

trait AtomType {
    // TODO: const when stable
    fn atom() -> Atom;
}

impl AtomType for u32 {
    fn atom() -> Atom {
        Atom(6)
    }
}

impl AtomType for u8 {
    fn atom() -> Atom {
        Atom(31)
    }
}

impl AtomType for Atom {
    fn atom() -> Atom {
        Atom(4)
    }
}

c_enum! { EventType -> u8 = [
    Mouse: 6,
    Expose: 12,
    Visibility: 15,
    Destroy: 17,
    Resize: 25,

    ClientMessage: 33,
    Generic: 35,

    Ignore: !0x80
] }
impl std::ops::BitAndAssign for EventType {
    fn bitand_assign(&mut self, rhs: Self) {
        self.0 &= rhs.0 
    }
}

use event::Event;
mod event {
    use super::*;
    #[repr(C)]
    pub union Any {
        generic: Generic,
        mouse: Mouse,
        expose: Expose,
        visibility: Visibility,
        destroy: Destroy,
        resize: Resize,
        client_message: ClientMessage
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Generic {
        response_type: EventType,
        _pad: u8,
        sequence: u16,
        _pad2: [u32; 7],
        full_sequence: u32
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Mouse {
        response_type: EventType,
        detail: u8,
        sequence: u16,
        time: Timestamp,
        root: Window,
        event: Window,
        child: Window,
        root_x: i16,
        root_y: i16,
        x: i16,
        y: i16,
        state: u16,
        same_screen: u8,
        _pad: u8
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Expose {
        response_type: EventType,
        _pad: u8,
        sequence: u16,
        window: Window,
        x: u16,
        y: u16,
        width: u16,
        height: u16,
        count: u16,
        _pad2: u16
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Visibility {
        response_type: EventType,
        _pad: u8,
        sequence: u16,
        window: Window,
        state: u8,
        _pad2: [u8; 3]
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Destroy {
        response_type: EventType,
        _pad: u8,
        sequence: u16,
        event: Window,
        window: Window,
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct Resize {
        response_type: EventType,
        _pad: u8,
        sequence: u16,
        window: Window,
        width: u16,
        height: u16,
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    pub union ClientMessageData {
        data8: [u8; 20],
        data16: [u16; 10],
        data32: [u32; 5]
    }
    impl ClientMessageData {
        pub fn atoms(&self) -> &[u32] {
            unsafe {
                &self.data32
            }
        }
    }
    impl std::fmt::Debug for ClientMessageData {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "Generic Client Message")
        }
    }

    #[derive(Copy, Clone)]
    #[repr(C)]
    struct ClientMessage {
        response_type: EventType,
        format: u8,
        sequence: u16,
        window: Window,
        atom: Atom,
        data: ClientMessageData,
    }

    #[derive(Debug)]
    pub enum Event {
        Mouse {
            root: Window,
            window: Window,
            root_x: i16,
            root_y: i16,
            x: i16,
            y: i16,
            same_screen: bool
        },
        Expose {
            window: Window,
            size: super::Size
        },
        Resize {
            window: Window,
            width: u16,
            height: u16,
        },
        Visibility {
            window: Window,
            state: u8
        },
        ClientMessage {
            window: Window,
            atom: Atom,
            data: ClientMessageData
        },
        Generic
    }
    impl Event {
        pub unsafe fn new(any_event: *mut Any) -> Option<Self> {
            if any_event.is_null() {
                None
            } else {
                (*any_event).generic.response_type &= EventType::Ignore;
                let event = match *any_event {
                    Any { mouse: Mouse { response_type: EventType::Mouse, root, event, root_x, root_y, x, y, same_screen, .. } } => Event::Mouse {
                        root,
                        window: event,
                        root_x,
                        root_y,
                        x,
                        y,
                        same_screen: same_screen != 0
                    },
                    Any { expose: Expose { response_type: EventType::Expose, window, x, y, width, height, .. } } => Event::Expose {
                        window,
                        size: Size {
                            x, y, width, height
                        }
                    },
                    Any { visibility: Visibility { response_type: EventType::Visibility, window, state, .. } } => Event::Visibility {
                        window,
                        state
                    },
                    Any { resize: Resize { response_type: EventType::Resize, window, width, height, .. } } => Event::Resize {
                        window,
                        width,
                        height
                    },
                    Any { client_message: ClientMessage { response_type: EventType::ClientMessage, window, atom, data, .. } } => Event::ClientMessage {
                        window,
                        atom,
                        data
                    },
                    Any { generic: Generic { response_type: EventType::Generic, .. } } => Event::Generic,
                    Any { generic: Generic { response_type, .. } } => panic!("Unknown event type ({:?}) received", response_type)
                };
        
                free(any_event as _);
        
                Some(event)
            }
        }
    }
}

type Result<T> = std::result::Result<T, Error>;
pub struct Error(u8);
use std::fmt;
impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error(code: {})", self.0)
    }
}
