use crate::ffi::*;

use super::event;



#[repr(C)]
pub struct Connection { _opaque: [u8; 0] }

#[repr(C)]
pub struct Cookie { _opaque: [u8; 0] }
impl Cookie {
    pub fn check(self, connection: *mut Connection) -> Result<(), super::Error> {
        unsafe {
            let error = xcb_request_check(connection, self);
            if error.is_null() {
                Ok(())
            } else {
                Err(super::Error((*error).error_code))
            }
        }
    }
}

pub type Window = u32;
pub type Visual = u32;
pub type Colourmap = u32;
pub type Timestamp = u32;
pub type Keycode = u8;


#[repr(C)]
pub struct Screen {
    pub root: Window,
    default_colourmap: Colourmap,
    white: u32,
    black: u32,
    input_masks: u32,
    width: u16,
    height: u16,
    width_millimeters: u16,
    height_millimeters: u16,
    min_installed_maps: u16,
    max_installed_maps: u16,
    pub root_visual: Visual,
    backing_stores: u8,
    save_unders: u8,
    root_depth: u8,
    allowed_depths_len: u8 
}

#[repr(C)]
pub struct Setup {
    status: u8,
    _pad0: u8,
    major_version: u16,
    minor_version: u16,
    length: u16,
    release: u32,
    resource_id_base: u32,
    resource_id_mask: u32,
    motion_buffer_size: u32,
    vendor_len: u16,
    max_request_length: u16,
    roots_len: u8,
    pixmap_formats_len: u8,
    image_byte_order: u8,
    bitmap_format_bit_order: u8,
    bitmap_format_scanline_unit: u8,
    bitmap_format_scanline_pad: u8,
    min_keycode: Keycode,
    max_keycode: Keycode,
    _pad1: [u8; 4]
}

#[repr(C)]
pub struct ScreenIterator<'a> {
    pub data: Option<&'a Screen>,
    rem: i32,
    index: i32
}

#[repr(C)]
pub struct InternAtomCookie {
    sequence: u32
}

#[repr(C)]
pub struct InternAtomReply {
    response_type: u8,
    _pad: u8,
    sequence: u16,
    length: u32,
    pub atom: Atom
}

#[repr(C)]
pub struct GeometryCookie {
    sequence: u32
}

#[repr(C)]
pub struct GeometryReply {
    response_type: u8,
    depth: u8,
    sequence: u16,
    length: u32,
    root: Window,
    pub x: u16,
    pub y: u16,
    pub width: u16,
    pub height: u16,
    border_width: u16,
    _pad: [u8; 2]
}

#[repr(C)]
pub struct PointerReply {
    response_type: u8,
    same_screen: u8,
    sequence: u16,
    length: u32,
    root: Window,
    child: Window,
    pub root_x: i16,
    pub root_y: i16,
    pub x: i16,
    pub y: i16,
    mask: u16,
    _pad: [u8; 2]
}

#[repr(C)]
pub struct Error {
    response_type: u8,
    pub error_code: u8,
    sequence: u16,
    resource_id: u32,
    minor_code: u16,
    major_code: u8,
    _pad: [u8; 21],
    full_sequence: u32
}

impl Error {
    pub fn none() -> Self {
        Self {
            response_type: 0,
            error_code: 0,
            sequence: 0,
            resource_id: 0,
            minor_code: 0,
            major_code: 0,
            _pad: [0; 21],
            full_sequence: 0
        }
    }
}

// Consts
pub const COPY_FROM_PARENT: u8 = 0;

c_enum! { Class -> u16 = [InputOutput: 0] }
c_enum! { Atom -> u32 = [WM_NAME: 39] }
c_enum! { AtomType -> u32 = [Atom: 4, Cardinal: 6, String: 31] }
c_enum! { PropertyMode -> u8 = [Replace: 0] }
c_enum! { ValueMask -> u32 = [None: 0, Event: 1 << 11]}
c_enum! { PointerCookie -> u32 = []}
impl std::ops::BitOr for ValueMask {
    type Output = u32;
    fn bitor(self, rhs: Self) -> u32 {
        self.0 | rhs.0
    }
}
impl std::ops::BitOr<ValueMask> for u32 {
    type Output = u32;
    fn bitor(self, rhs: ValueMask) -> u32 {
        self | rhs.0
    }
}

c_enum! { EventMask -> u32 = [
    None: 0,
    KeyPress: 1,
    KeyRelease: 1 << 1,
    ButtonPress: 1 << 2,
    Buttonrelease: 1 << 3,
    EnterWindow: 1 << 4,
    LeaveWindow: 1 << 5,
    MouseMotion: 1 << 6,
    MouseMotionHint: 1 << 7,

    Expose: 1 << 15,
    Visibility: 1 << 16,

    Resize: 1 << 18,
    Notify: 1 << 19
] }
impl std::ops::BitOr for EventMask {
    type Output = u32;
    fn bitor(self, rhs: Self) -> u32 {
        self.0 | rhs.0
    }
}
impl std::ops::BitOr<EventMask> for u32 {
    type Output = u32;
    fn bitor(self, rhs: EventMask) -> u32 {
        self | rhs.0
    }
}