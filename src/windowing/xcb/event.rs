use crate::c_enum;
use super::clib::*;

c_enum! { EventType -> u8 = [
    Mouse: 6,
    Expose: 12,
    Visibility: 15,
    Destroy: 17,
    Resize: 25,

    ClientMessage: 33,
    Generic: 35,

    Ignore: !0x80
] }
impl std::ops::BitAndAssign for EventType {
    fn bitand_assign(&mut self, rhs: Self) {
        self.0 &= rhs.0 
    }
}

#[repr(C)]
pub union Any {
    generic: Generic,
    mouse: Mouse,
    expose: Expose,
    visibility: Visibility,
    destroy: Destroy,
    resize: Resize,
    client_message: ClientMessage
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Generic {
    response_type: EventType,
    _pad: u8,
    sequence: u16,
    _pad2: [u32; 7],
    full_sequence: u32
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Mouse {
    response_type: EventType,
    detail: u8,
    sequence: u16,
    time: Timestamp,
    root: Window,
    event: Window,
    child: Window,
    root_x: i16,
    root_y: i16,
    x: i16,
    y: i16,
    state: u16,
    same_screen: u8,
    _pad: u8
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Expose {
    response_type: EventType,
    _pad: u8,
    sequence: u16,
    window: Window,
    x: u16,
    y: u16,
    width: u16,
    height: u16,
    count: u16,
    _pad2: u16
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Visibility {
    response_type: EventType,
    _pad: u8,
    sequence: u16,
    window: Window,
    state: u8,
    _pad2: [u8; 3]
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Destroy {
    response_type: EventType,
    _pad: u8,
    sequence: u16,
    event: Window,
    window: Window,
}

#[derive(Copy, Clone)]
#[repr(C)]
struct Resize {
    response_type: EventType,
    _pad: u8,
    sequence: u16,
    window: Window,
    width: u16,
    height: u16,
}

#[derive(Copy, Clone)]
#[repr(C)]
pub union ClientMessageData {
    data8: [u8; 20],
    data16: [u16; 10],
    data32: [u32; 5]
}
impl ClientMessageData {
    pub fn atoms(&self) -> &[u32] {
        unsafe {
            &self.data32
        }
    }
}
impl std::fmt::Debug for ClientMessageData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Generic Client Message")
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
struct ClientMessage {
    response_type: EventType,
    format: u8,
    sequence: u16,
    window: Window,
    atom: Atom,
    data: ClientMessageData,
}

#[derive(Debug)]
pub enum Event {
    Mouse {
        root: Window,
        window: Window,
        root_x: i16,
        root_y: i16,
        x: i16,
        y: i16,
        same_screen: bool
    },
    Expose {
        window: Window,
        size: super::Size
    },
    Resize {
        window: Window,
        width: u16,
        height: u16,
    },
    Visibility {
        window: Window,
        state: u8
    },
    ClientMessage {
        window: Window,
        atom: Atom,
        data: ClientMessageData
    },
    Generic
}
impl Event {
    pub unsafe fn new(any_event: *mut Any) -> Option<Self> {
        use super::Size;
        if any_event.is_null() {
            None
        } else {
            (*any_event).generic.response_type &= EventType::Ignore;
            let event = match *any_event {
                Any { mouse: Mouse { response_type: EventType::Mouse, root, event, root_x, root_y, x, y, same_screen, .. } } => Event::Mouse {
                    root,
                    window: event,
                    root_x,
                    root_y,
                    x,
                    y,
                    same_screen: same_screen != 0
                },
                Any { expose: Expose { response_type: EventType::Expose, window, x, y, width, height, .. } } => Event::Expose {
                    window,
                    size: Size {
                        x, y, width, height
                    }
                },
                Any { visibility: Visibility { response_type: EventType::Visibility, window, state, .. } } => Event::Visibility {
                    window,
                    state
                },
                Any { resize: Resize { response_type: EventType::Resize, window, width, height, .. } } => Event::Resize {
                    window,
                    width,
                    height
                },
                Any { client_message: ClientMessage { response_type: EventType::ClientMessage, window, atom, data, .. } } => Event::ClientMessage {
                    window,
                    atom,
                    data
                },
                Any { generic: Generic { response_type: EventType::Generic, .. } } => Event::Generic,
                Any { generic: Generic { response_type, .. } } => panic!("Unknown event type ({:?}) received", response_type)
            };
    
            free(any_event as _);
    
            Some(event)
        }
    }
}