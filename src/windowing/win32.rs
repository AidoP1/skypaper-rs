use crate::prelude::*;
use crate::ffi::*;
use crate::c_enum;

pub struct Win32 {
    connection: *const Connection,
    window: *mut Window,
    running: i32,
    message: Message
}

impl Win32 {
    pub fn new() -> Result<Self> {
        let connection = unsafe { GetModuleHandleA(null()) };
        if connection.is_null() {
            panic!("Unable to get main instance")
        }
        let class_name = &[b'S' as u16, b'k' as _, b'y' as _, b'p' as _, b'a' as _, b'p' as _, b'e' as _, b'r' as _] as _;
        let win_class = WindowClass {
            style: 0,
            window_process: window_proc,
            extra_bytes_class: 0,
            extra_bytes_instance: 0,
            instance_handle: connection,
            icon_handle: null(),
            cursor_handle: null(),
            brush_handle: null(),
            menu_name: null(),
            class_name,
        };

        let class = unsafe { RegisterClassA(&win_class as _) };

        let window = unsafe { CreateWindowExA(0, class_name, b"Window\0" as _, WS_POPUP, 0, 0, 0, 0, null(), null(), connection, null()) };
        if window.is_null() { panic!("Failed to create window") }
        unsafe {
            ShowWindow(window, 3);
            SetWindowPos(window, BOTTOM_WINDOW, 0, 0, 0, 0, WINDOW_FLAGS);
        }

        Ok(Self {
            connection,
            window,
            running: 0,
            message: unsafe { std::mem::zeroed() }
        })
    }

    pub fn size(&self) -> Result<Size> {
        Ok(Size {
            x: 0,
            y: 0,
            width: 1920,
            height: 1080
        })
    }

    pub fn mouse_position(&self) -> Option<MousePosition> {
        let mut pos = Point {
            x: 0,
            y: 0
        };
        if unsafe { GetCursorPos(&mut pos) } != 0 {
            Some(MousePosition { x: pos.x, y: pos.y })
        } else {
            None
        }
    }

    pub fn handles(&self) -> (*const Connection, *mut Window) {
        (self.connection, self.window)
    }

    pub fn event_pump(&mut self) -> Option<super::Event> {
        self.running = unsafe { PeekMessageA(&mut self.message as _, null(), 0, 0, 1) };
        if self.running != 0 {
            unsafe {
                TranslateMessage(&mut self.message as _);
                DispatchMessageA(&mut self.message as _);
            }
            // WM_QUIT https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-quit
            Some(if self.message.message == 0x12 { super::Event::Quit } else { super::Event::Nothing })
        } else {
            None
        }
    }
}


extern "system" fn window_proc(window_handle: *mut Window, message: u32, wparam: *mut Void, lparam: *mut Void) -> i64 {
    if message == DESTROY {
        unsafe { PostQuitMessage(0) };
        0
    } else if message == PAINT {
        let mut ps = PaintStruct {
            hdc: null(),
            erase: 0,
            rc_paint: Rect {
                top: 0,
                bottom: 0,
                left: 0,
                right: 0
            },
            restore: 0,
            _res: [0; 32],
            _upd: 0
        };
        unsafe {
            let hdc = BeginPaint(window_handle, &mut ps as _);
            FillRect(hdc, &mut ps.rc_paint as _, BrushColour::Background);
            EndPaint(window_handle, &mut ps as _);
        }
        0
    } else if message == POS_CHANGING {
        unsafe { (*(lparam as *mut WindowPos)).flags |= NO_ZORDER };
        0
    } else {
        unsafe { DefWindowProcA(window_handle, message, wparam, lparam) }
    }
}

#[link(name = "user32")]
extern "system" {
    fn CreateWindowExA(ex_style: u32, class_name: *const u16, window_name: *const u8, style: u32, x: i32, y: i32, width: i32, height: i32, parent: *const Unused, menu: *const Unused, instance: *const Connection, param: *const Unused) -> *mut Window;
    fn GetModuleHandleA(module: *const Void) -> *const Connection;
    fn ShowWindow(window: *mut Window, num: i32);
    fn RegisterClassA(class: *const WindowClass) -> Atom;
    fn DefWindowProcA(window: *mut Window, message: u32, wparam: *const Void, lparam: *const Void) -> i64;
    fn PeekMessageA(message: *mut Message, window: *const Window, filter_min: u32, filter_max: u32, remove: u32) -> i32;
    fn TranslateMessage(message: *mut Message);
    fn DispatchMessageA(message: *mut Message);
    fn PostQuitMessage(code: i32);
    fn SetWindowPos(window: *mut Window, insert_after: *mut Window, x: i32, y: i32, cx: i32, cy: i32, flags: u32) -> u8;

    fn GetCursorPos(position: *mut Point) -> i32;

    fn BeginPaint(window: *mut Window, ps: *mut PaintStruct) -> *mut DisplayContext;
    fn FillRect(hdc: *mut DisplayContext, rcpaint: *const Rect, brush: BrushColour);
    fn EndPaint(window: *mut Window, ps: *mut PaintStruct);
}

#[repr(C)]
pub struct Window { _opaque: [u8; 0] }
#[repr(C)]
pub struct Connection { _opaque: [u8; 0] }

const BOTTOM_WINDOW: *mut Window = 1 as _;
const WINDOW_FLAGS: u32 = 1 | 2 | 0x10 | 0x200 | 0x400;
const NO_ZORDER: u32 = 4;
const WS_POPUP: u32 = 0x8000_0000;

const POS_CHANGING: u32 = 0x46;
const DESTROY: u32 = 2;
const PAINT: u32 = 0xF;

#[repr(transparent)]
struct Unused { __: u8 }

#[repr(transparent)]
struct Atom { __: u32 }
#[repr(C)]
struct DisplayContext { __: u32 }

type WindowProcess = extern "system" fn(*mut Window, u32, *mut Void, *mut Void) -> i64;

#[repr(C)]
struct WindowClass {
    style: u32,
    window_process: WindowProcess,
    extra_bytes_class: i32,
    extra_bytes_instance: i32,
    instance_handle: *const Connection,
    icon_handle: *const Unused,
    cursor_handle: *const Unused,
    brush_handle: *const Unused,
    menu_name: *const u16,
    class_name: *const u16,
}

#[repr(C)]
struct Message {
    window: *mut Window,
    message: u32,
    wparam: *const Void,
    lparam: *const Void,
    time: u64,
    curs_x: i64,
    curs_y: i64,
    private: u64
}

#[repr(C)]
struct WindowPos {
    window: *const Window,
    insert_after: *const Window,
    x: i32,
    y: i32,
    cx: i32,
    cy: i32,
    flags: u32
}

#[derive(Debug)]
#[repr(C)]
struct Rect {
    left: i32,
    top: i32,
    right: i32,
    bottom: i32
}

#[derive(Debug)]
#[repr(C)]
struct Point {
    x: i32,
    y: i32,
}

#[repr(C)]
struct PaintStruct {
    hdc: *const DisplayContext,
    erase: i32,
    rc_paint: Rect,
    restore: i32,
    _upd: i32,
    _res: [u8; 32]
}

c_enum! { BrushColour -> usize = [Background: 6] }

type Result<T> = std::result::Result<T, Error>;
pub struct Error(u8);

use std::fmt;
impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error(code: {})", self.0)
    }
}
